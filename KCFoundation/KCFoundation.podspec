Pod::Spec.new do |s|
  s.name         = "KCFoundation"
  s.version      = "0.0.1"
  s.summary      = "KC Foundation"
  s.description  = "KC Foundation For Test"

  s.homepage     = "https://bitbucket.org/kcpod/kcfoundation"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author             = { "Kennix" => "Kennix@com" }

  s.platform     = :ios, "8.0"

  s.source       = { :git => "https://kennix426@bitbucket.org/kcpod/kcfoundation.git", :tag => "0.0.1" }
  # s.source_files  = "KCFoundation/*.{h,m,swift}"

  s.subspec 'KCCoreKit' do |spec|
    spec.source_files   = "KCCoreKit/Source/*.{h,m,swift}"
  end

  # s.xcconfig = { "HEADER_SEARCH_PATHS" => "$(SDKROOT)/usr/include/libxml2" }

end
