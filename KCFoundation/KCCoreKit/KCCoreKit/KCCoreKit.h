//
//  KCCoreKit.h
//  KCCoreKit
//
//  Created by Kennix Chui on 8/30/16.
//  Copyright © 2016 Kennix. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for KCCoreKit.
FOUNDATION_EXPORT double KCCoreKitVersionNumber;

//! Project version string for KCCoreKit.
FOUNDATION_EXPORT const unsigned char KCCoreKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <KCCoreKit/PublicHeader.h>


